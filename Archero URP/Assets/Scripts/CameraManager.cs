﻿using UnityEngine;
using UnityEditor;
using Cinemachine;

namespace Assets.Scripts
{
    public class CameraManager : MonoBehaviour
    {
        public GameObject environment;

        void Start()
        {
            CinemachineVirtualCamera cam = GetComponent<CinemachineVirtualCamera>();
            cam.m_Lens.OrthographicSize = environment.GetComponent<MeshCollider>().bounds.size.x * Screen.height / Screen.width * 0.5f;
        }
    }
}