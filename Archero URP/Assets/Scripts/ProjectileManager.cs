﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ProjectileManager : MonoBehaviour
{
    [SerializeField]
    private float speed = 100;

    [SerializeField]
    private int damage = 15;
    public int Damage { set { damage = value; } get { return damage; } }

    private Vector3 forward;

    private void OnEnable()
    {
        forward = transform.TransformDirection(Vector3.forward);
    }

    private void Update()
    {
        transform.position += transform.forward * Time.deltaTime * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        gameObject.SetActive(false);
    }
}
