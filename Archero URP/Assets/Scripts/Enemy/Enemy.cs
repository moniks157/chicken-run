﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

namespace ArcheroURP.Enemy
{
    public class Enemy : MonoBehaviour
    {
        protected Animator anim;
        protected Transform player;
        protected NavMeshAgent agent;
        protected bool dead;

        [SerializeField]
        protected int damage;

        private int id;
        public int Id { set { id = value; } get { return id; } }

        private int attackDamage;
        public int AttackDamage { set { attackDamage = value; } get { return attackDamage; } }

        private int collisionDamage = 5;
        public int CollisionDamage { set { collisionDamage = value; } get { return collisionDamage; } }

        private Collider[] hitColliders;

        protected void Start()
        {
            anim = GetComponentInChildren<Animator>();
            player = GameObject.Find("Player").transform;
            agent = GetComponent<NavMeshAgent>();
            attackDamage = damage;
        }

        private void OnEnable()
        {
            GameEvents.Instance.onDeath += OnDeath;
            dead = false;
        }

        private void OnDisable()
        {
            GameEvents.Instance.onDeath -= OnDeath;
        }

        private void OnDestroy()
        {
            GameEvents.Instance.onDeath -= OnDeath;
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Projectile" && !dead)
            {
                int damage = other.GetComponent<ProjectileManager>().Damage;
                anim.SetTrigger("Take Damage");
                GameEvents.Instance.ShotTrigger(Id, damage);
            }
            else if (other.tag == "Player" && !dead)
            {
                GameEvents.Instance.ShotTrigger(0, collisionDamage);
            }
        }

        private void OnDeath(int id)
        {
            if(id == this.Id)
            {
                StopAllCoroutines();
                StartCoroutine(DeathCourutine());
            }
        }

        protected virtual IEnumerator DeathCourutine()
        {
            anim.SetBool("Run Forward", false);
            anim.SetTrigger("Die");

            dead = true;
            yield return new WaitForSeconds(1.5f);
            gameObject.SetActive(false);
        }
    }
}
