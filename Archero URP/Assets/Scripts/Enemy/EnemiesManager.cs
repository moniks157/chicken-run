﻿using System.Collections.Generic;
using UnityEngine;
using ArcheroURP.Controllers;

namespace ArcheroURP.Enemy
{
    public class EnemiesManager : Singleton<EnemiesManager>
    {
        public List<GameObject> enemies = new List<GameObject>();

        private void Start()
        {
            SetUpEnemies();
        }

        private void SetUpEnemies()
        {
            if(enemies.Count > 0 )
            {
                for(int i = 0; i < enemies.Count; i++)
                {
                    enemies[i].GetComponent<Enemy>().Id = i + 100;
                    enemies[i].GetComponent<Health>().Id = i + 100;
                }
            }
        }
    }
}