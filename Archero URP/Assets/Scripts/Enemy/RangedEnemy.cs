﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ArcheroURP.Enemy
{
    public class RangedEnemy : Enemy
    {
        private LineRenderer lineRenderer;
 
        private Vector3 playerDirection;
        private Vector3 up = new Vector3(0f, 0.1f, 0f);

        private bool shooting = false;

        private RaycastHit hit;
        private LayerMask wallMask = 1 << 10;
        private LayerMask playerMask = 1 << 9;

        

        [SerializeField]
        private Material leserBeamMat;
        [SerializeField]
        private Material trackerMat;

        [SerializeField]
        private List<Transform> patrolPoints = new List<Transform>();
        private Queue<Transform> patrolPointsQueue;

        private System.Random rand;

        private void Start()
        {
            base.Start();
            lineRenderer = GetComponentInChildren<LineRenderer>();
            patrolPointsQueue = new Queue<Transform>(patrolPoints);

            rand = new System.Random();

            SetPositions(3);

            StartCoroutine(Patrol((float)(rand.NextDouble()+3f)));
        }

        private void Update()
        {
            if (shooting)
            {
                playerDirection = (player.position - transform.position).normalized;
                Quaternion rotation = new Quaternion();
                rotation.SetLookRotation(playerDirection);
                Physics.Raycast(transform.position, playerDirection, out hit,  20, wallMask);
                if (hit.collider)
                {
                    lineRenderer.SetPosition(0, transform.position + up);
                    lineRenderer.SetPosition(1, player.position + up);
                    lineRenderer.SetPosition(2, hit.point + up);

                    transform.rotation = Quaternion.RotateTowards(transform.rotation,rotation,270f*Time.deltaTime);
                }
            }
        }

        private IEnumerator StartShooting()
        {
            yield return new WaitForSeconds(3f);
            StartCoroutine(Shoot());
        }

        protected override IEnumerator DeathCourutine()
        {
            //StopAllCoroutines();
            agent.isStopped = true;
            
            anim.SetBool("Walk Forward", false);
            anim.SetTrigger("Die");

            dead = true;
            yield return new WaitForSeconds(1.5f);
            gameObject.SetActive(false);
        }

        private IEnumerator Shoot()
        {
            shooting = true;
            anim.SetBool("Walk Forward", false);
            yield return new WaitForSeconds(2f);
            shooting = false;
            yield return new WaitForSeconds(0.5f);
            
            lineRenderer.startColor = Color.magenta;
            lineRenderer.endColor = Color.magenta;

            Physics.Raycast(transform.position + Vector3.up * .15f, transform.rotation * Vector3.forward , out hit,Mathf.Infinity, playerMask);
            if (hit.collider)
            {
                GameEvents.Instance.ShotTrigger(0, AttackDamage);
            }

            yield return new WaitForSeconds(0.5f);
            SetPositions(3);
            StartCoroutine(Patrol((float)(rand.NextDouble() + 3f)));
        }

        private IEnumerator Patrol(float seconds)
        {

            if (patrolPoints.Count > 0)
            {
                agent.isStopped = false;
                anim.SetBool("Walk Forward", true);
                Transform destinstion = patrolPointsQueue.Dequeue();

                agent.SetDestination(destinstion.position);

                yield return new WaitForSeconds(seconds);

                patrolPointsQueue.Enqueue(destinstion);

                agent.isStopped = true;
                anim.SetBool("Walk Forward", false) ;
            }
            else
            {
                transform.LookAt(player);
                yield return new WaitForSeconds(seconds);
            }
            StartCoroutine(Shoot());
        }

        private void SetPositions(int positionsCount)
        {
            lineRenderer.positionCount = positionsCount;
            for(int i = 0; i< positionsCount; i++)
                lineRenderer.SetPosition(i, transform.position);
            lineRenderer.material = trackerMat;
            lineRenderer.startColor = Color.gray;
            lineRenderer.endColor = Color.gray;
        }
    }
}