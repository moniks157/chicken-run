﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace ArcheroURP.Enemy
{
    public class MeleeEnemy : Enemy
    {
        private void Start()
        {
            base.Start();

            StartCoroutine(StartMoving(2f));
        }

        protected override IEnumerator DeathCourutine()
        {
            agent.isStopped = true;
            anim.SetBool("Run Forward", false);
            anim.SetTrigger("Die");

            dead = true;
            yield return new WaitForSeconds(1.5f);
            gameObject.SetActive(false);
        }

        private IEnumerator StartMoving(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            StartCoroutine(Move());
        }

        private IEnumerator Move()
        {
            agent.isStopped = false;
            anim.SetBool("Run Forward", true);
            agent.SetDestination(player.position);
            yield return new WaitForSeconds(1f);
            if (!dead)
                StartCoroutine(Move());               
        }

        private IEnumerator Attack()
        {
            agent.isStopped = true;
            anim.SetTrigger("Attack 02");
            GameEvents.Instance.ShotTrigger(0, AttackDamage);
            yield return new WaitForSeconds(.5f);
            agent.isStopped = false;
        }

        protected override void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Projectile" && !dead)
            {
                StopAllCoroutines();
                agent.isStopped = true;
                anim.SetBool("Run Forward", false);
                int damage = other.GetComponent<ProjectileManager>().Damage;
                
                GameEvents.Instance.ShotTrigger(Id, damage);
                if (!dead)
                {
                    anim.SetTrigger("Take Damage");
                    StartCoroutine(StartMoving(.5f));
                }
            }
            if (other.tag == "Player" && !dead)
            {
                StopAllCoroutines();
                StartCoroutine(Attack());
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.tag == "Player" && !dead)
            {
                StartCoroutine(StartMoving(.5f));
            }
        }
    }
}
