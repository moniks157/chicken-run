﻿using ArcheroURP;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArcheroURP
{
    public class GameEvents : Singleton<GameEvents>
    {
        private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        public event Action<int, int> onShotTrigger;
        public void ShotTrigger(int id, int damage)
        {
            if (onShotTrigger != null)
            {
                onShotTrigger(id, damage);
            }
        }

        public event Action<int> onDeath;
        public void Death(int id)
        {
            if (onDeath != null)
            {
                onDeath(id);
            }
        }

        public event Action onEnemiesSetUp;
        public void EnemiesSetUp()
        {
            if (onEnemiesSetUp != null)
            {
                onEnemiesSetUp();
            }
        }
    }
}
