﻿using UnityEngine;
using UnityEditor;

namespace ArcheroURP.GUI
{
    public class GUI : MonoBehaviour
    {
        [SerializeField]
        private GameObject mainMenu;

        [SerializeField]
        private GameObject gameOver;

        private void Start()
        {
            if (GameManager.Instance.endgame)
            {
                mainMenu.SetActive(false);
                gameOver.SetActive(true);
            }
            else
            {
                mainMenu.SetActive(true);
                gameOver.SetActive(false);
            }
        }

    }
}