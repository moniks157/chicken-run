﻿using UnityEngine;
using UnityEditor;
using TMPro;

namespace ArcheroURP.GUI
{
    public class GameOver : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI txt;

        private void OnEnable()
        {
            if(GameManager.Instance.gameover)
            {
                txt.text = "Game Over";
            }
            else
            {
                txt.text = "You Win";
            }
        }
    }
}