﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.GUI
{
    public class MainMenu: MonoBehaviour
    {

        public void Play()
        {
            StartCoroutine(LoadYourAsyncScene());
        }

        private IEnumerator LoadYourAsyncScene()
        { 
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(1);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }
        }

        public void Exit()
        {
            Application.Quit();
        }
    }
}