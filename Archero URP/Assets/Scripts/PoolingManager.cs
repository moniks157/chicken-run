﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ArcheroURP
{
    public enum Projectile
    {
        Dagger
    }

    public class PoolingManager : Singleton<PoolingManager>
    {
        [Serializable]
        public class Pool
        {
            public Projectile tag;
            public List<GameObject> prefabs;
            public int size;
        }

        public List<Pool> pools;
        public Dictionary<Projectile, Queue<GameObject>> poolDictionary;

        [HideInInspector]
        public bool isInitialized = false;

        private void Start()
        {
            poolDictionary = new Dictionary<Projectile, Queue<GameObject>>();

            foreach (Pool pool in pools)
            {
                Queue<GameObject> objectPool = new Queue<GameObject>();

                System.Random random = new System.Random();
                for (int i = 0; i < pool.size; i++)
                {
                    int r = random.Next(pool.prefabs.Count);
                    GameObject obj = Instantiate(pool.prefabs[r]);
                    obj.SetActive(false);
                    objectPool.Enqueue(obj);
                }
                poolDictionary.Add(pool.tag, objectPool);
            }

            isInitialized = true;
        }

        public GameObject SpawnFromPool(Projectile tag, Vector3 position, Quaternion rotation)
        {
            if(!poolDictionary.ContainsKey(tag))
            {
                Debug.LogWarning("Pool with tag " + tag + " doesn't exist.");
                return null;
            }

            GameObject objectToSpawn = poolDictionary[tag].Dequeue();

            objectToSpawn.SetActive(true);
            objectToSpawn.transform.position = position;
            objectToSpawn.transform.rotation = rotation;

            poolDictionary[tag].Enqueue(objectToSpawn);
            return objectToSpawn;
        }
    }
}
