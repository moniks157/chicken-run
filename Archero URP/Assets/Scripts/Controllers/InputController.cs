﻿using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

namespace ArcheroURP.Controllers
{
    [Serializable]
    public class MoveInputEvent : UnityEvent<float, float> { }

    [Serializable]
    public class StopedMoveInputEvent : UnityEvent { }

    public class InputController : MonoBehaviour
    {
        Controls controls;
        public MoveInputEvent moveInputEvent;
        public StopedMoveInputEvent stopedMoveInputEvent;

        private void Awake()
        {
            controls = new Controls();
        }

        private void OnEnable()
        {
            controls.Gameplay.Enable();

            controls.Gameplay.Move.performed += OnMovePerformed;
            controls.Gameplay.Move.canceled += OnMovePerformed;
            controls.Gameplay.Move.canceled += OnMoveStopped;
        }

        private void OnMovePerformed(InputAction.CallbackContext context)
        {
            Vector2 moveInput = context.ReadValue<Vector2>();
            moveInputEvent.Invoke(moveInput.x, moveInput.y);
        }

        private void OnMoveStopped(InputAction.CallbackContext context)
        {
            stopedMoveInputEvent.Invoke();
        }
    }
}
