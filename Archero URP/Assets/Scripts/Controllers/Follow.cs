﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ArcheroURP
{
    public class Follow : MonoBehaviour
    {
        [SerializeField]
        private GameObject objectToFollow;

        //private RectTransform transform;

        private Camera camera;

        private void Start()
        {
            camera = Camera.main;
            //transform = GetComponent<RectTransform>();
        }

        void Update()
        {
            transform.LookAt(transform.position + camera.transform.rotation * Vector3.back, camera.transform.rotation * Vector3.up);

            transform.position = objectToFollow.transform.position + Vector3.up * 0.75f;
        }
    }
}
