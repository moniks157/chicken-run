﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ArcheroURP.Enemy;
using System;
using UnityEngine.SceneManagement;

namespace ArcheroURP.Controllers
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField]
        private float moveSpeed = 5.0f;

        [SerializeField]
        private float rotationSpeed = 200.0f;

        [SerializeField]
        private float shootingSpeed = 5.0f;

        [SerializeField]
        private Transform projectileOrigin;

        private float horizontal;
        private float vertical;

        private CharacterController controller;
        private Animator anim;

        private bool moving;

        private List<GameObject> enemies;
        private GameObject targetEnemy;

        private PoolingManager pooling;

        private WaitForSeconds shootingWFS;

        private void Start()
        {
            controller = GetComponent<CharacterController>();
            anim = GetComponentInChildren<Animator>();

            moving = false;

            enemies = EnemiesManager.Instance.enemies;
            targetEnemy = GetClosestEnemy(enemies);
            GameEvents.Instance.onDeath += RemoveEnemyFromList;
            GameEvents.Instance.onDeath += OnDeath;

            pooling = PoolingManager.Instance;
            shootingWFS = new WaitForSeconds(shootingSpeed);
            StartCoroutine(StartShooting());
        }

        private void OnDeath(int id)
        {
            if (id == 0)
            {
                GameManager.Instance.endgame = true;
                GameManager.Instance.gameover = true;

                StartCoroutine(LoadYourAsyncScene());
            }
        }
        private IEnumerator LoadYourAsyncScene()
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(0);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }
        }

        private void Update()
        {
            Vector3 rotationDirection = Quaternion.AngleAxis(0, Vector3.up) * (Vector3.forward * vertical + Vector3.right * horizontal);
            Vector3 forward = controller.transform.TransformDirection(Vector3.forward);
            Quaternion rotation = new Quaternion();

            if (moving || enemies.Count == 0)
            {
                if (!rotationDirection.Equals(Vector3.zero))
                    rotation.SetLookRotation(rotationDirection);
                else
                    rotation.SetLookRotation(forward);
            }
            else
            {
                rotation.SetLookRotation(targetEnemy.transform.position - controller.transform.position);
            }            
            
            controller.transform.rotation = Quaternion.RotateTowards(controller.transform.rotation, rotation, rotationSpeed * Time.deltaTime);

            if (moving)
            {
                controller.Move(forward * Time.deltaTime * moveSpeed);
            }
            anim.SetBool("Run", moving);
        }

        public void OnMoveInput(float horizontal, float vertical)
        {
            this.horizontal = horizontal;
            this.vertical = vertical;
            moving = horizontal != 0 || vertical != 0;

            if (moving || enemies.Count == 0)
                StopAllCoroutines();
        }

        public void OnMoveStopped()
        {
            moving = false;
            targetEnemy = GetClosestEnemy(enemies);
            StartCoroutine(StartShooting());
        }

        private IEnumerator StartShooting()
        {
            yield return new WaitForSeconds(0.2f);
            if (!moving && enemies.Count != 0)
                StartCoroutine(Shoot());
        }

        private IEnumerator Shoot()
        {
            pooling.SpawnFromPool(Projectile.Dagger, projectileOrigin.position, projectileOrigin.rotation);
            yield return shootingWFS;
            if (!moving && enemies.Count != 0)
                StartCoroutine(Shoot());
        }

        private GameObject GetClosestEnemy(List<GameObject> enemies)
        {
            GameObject bestTarget = null;
            float closestDistanceSqr = Mathf.Infinity;
            Vector3 currentPosition = controller.transform.position;
            foreach (GameObject potentialTarget in enemies)
            {
                Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
                float dSqrToTarget = directionToTarget.sqrMagnitude;
                if (dSqrToTarget < closestDistanceSqr)
                {
                    closestDistanceSqr = dSqrToTarget;
                    bestTarget = potentialTarget;
                }
            }
            return bestTarget;
        }

        private void RemoveEnemyFromList(int id)
        {
            GameObject enemyToRemove = null;

            foreach (GameObject enemy in enemies)
            {
                int eId = enemy.GetComponentInParent<Enemy.Enemy>().Id;
                if (eId == id)
                {
                    enemyToRemove = enemy;
                }
            }

            enemies.Remove(enemyToRemove);

            if(enemies.Count == 0)
            {
                GameManager.Instance.endgame = true;
                GameManager.Instance.gameover = false;

                StartCoroutine(LoadYourAsyncScene());
            }
            targetEnemy = GetClosestEnemy(enemies);
        }

        private void OnDestroy()
        {
            GameEvents.Instance.onDeath -= RemoveEnemyFromList;
            GameEvents.Instance.onDeath -= OnDeath;
        }

        private void OnDisable()
        {
            GameEvents.Instance.onDeath -= OnDeath;
        }
    }
}
