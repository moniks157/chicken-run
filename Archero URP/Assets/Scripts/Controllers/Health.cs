﻿using UnityEngine;
using UnityEngine.UI;

namespace ArcheroURP.Controllers
{
    public class Health : MonoBehaviour
    {

        [SerializeField]
        private Image healthBar;

        [SerializeField]
        private int maxHealth = 100;

        [HideInInspector]
        public int currentHealth;

        private int id = 0;
        public int Id { set { id = value; } get { return id; } }

        private void Start()
        {
            GameEvents.Instance.onShotTrigger += ReduceHealth;
            currentHealth = maxHealth;
        }

        private void ReduceHealth(int id, int damage)
        {
            if (id == this.id)
            {
                healthBar.fillAmount -= DamagePct(damage);
                currentHealth -= damage;
                if (currentHealth <= 0)
                {
                    GameEvents.Instance.Death(id);
                }
            }
        }

        private float DamagePct(int damage)
        {
            float damagePct = 0f;
            damagePct = (float) damage / (float) maxHealth;
            
            return damagePct;
        }

        private void OnDestroy()
        {
            GameEvents.Instance.onShotTrigger -= ReduceHealth;
        }
    }
}
